package com.example.srin.minitwitteryamba.adapter;

import android.content.Context;
import android.database.Cursor;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.example.srin.minitwitteryamba.R;
import com.example.srin.minitwitteryamba.util.StatusData;

/**
 * Created by SRIN on 4/22/2016.
 */
public class FeedAdapter extends SimpleCursorAdapter {

    static final String[] from = {StatusData.C_CREATED_AT, StatusData.C_USER, StatusData.C_TEXT};
    static final int[] to = {R.id.text_created_at, R.id.text_user, R.id.text_post};

    public FeedAdapter(Context context, Cursor c) {
        super(context, R.layout.row, c, from, to);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        super.bindView(view, context, cursor);
        long timeStamp = cursor.getLong(cursor.getColumnIndex(StatusData.C_CREATED_AT));
        TextView textCreatedAt = (TextView) view.findViewById(R.id.text_created_at);
        textCreatedAt.setText(DateUtils.getRelativeTimeSpanString(timeStamp));
    }
}
