package com.example.srin.minitwitteryamba.activity;

import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.example.srin.minitwitteryamba.controller.MniTwitterApplication;
import com.example.srin.minitwitteryamba.R;
import com.example.srin.minitwitteryamba.services.GcmRegistrationServices;
import com.example.srin.minitwitteryamba.services.UpdaterService;
import com.example.srin.minitwitteryamba.fragment.page.FeedFragment;
import com.example.srin.minitwitteryamba.fragment.page.PostFragment;
import com.example.srin.minitwitteryamba.fragment.page.ProfileFragment;
import com.example.srin.minitwitteryamba.util.GcmUtil;

public class HomeActivity extends AppCompatActivity {

    private static final String TAG = HomeActivity.class.getSimpleName();
    private static final int FEED_CONSTANT = 0;
    private static final int INITIAL_POSITION = -1;
    private static final int ADD_POST = 1;
    private static final int PROFILE_CONSTANT = 2;

    private Toolbar toolbar;

    private ProfileFragment profileFragment;
    private FeedFragment feedFragment;
    private PostFragment postFragment;

    private MniTwitterApplication twit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        twit = (MniTwitterApplication) getApplication();
        setToolbar();
        setNavigationBottom();
        initializeGcm();
        replacepage(getSupportFragmentManager(), INITIAL_POSITION, FEED_CONSTANT);
        startService(new Intent(this, UpdaterService.class));
        Log.v("testing runing", twit.isServiceRunning() + "");
    }

    private void initializeGcm(){
        Log.v(TAG, "masuk initializeGcm method");
        if (GcmUtil.checkPlayServices(this)){
            Log.v(TAG, "masuk if initialize gcm method");
            Intent intent = new Intent(this, GcmRegistrationServices.class);
            startService(intent);
        }else {
            Log.v(TAG, "masuk else initialize gcm method");
            Log.v(TAG, "No valid google play service apk found");
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    public void setToolbar(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setElevation(20);
        setSupportActionBar(toolbar);
    }

    public void setNavigationBottom(){
        AHBottomNavigation bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);

// Create items
        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.tab_1, android.R.drawable.ic_dialog_info, R.color.colorPrimary);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.tab_2, android.R.drawable.ic_input_add, R.color.pressed);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.tab_3, android.R.drawable.ic_popup_sync, R.color.dark_blue);

// Add items
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);

// Set background color
        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#FEFEFE"));

// Disable the translation inside the CoordinatorLayout
        bottomNavigation.setBehaviorTranslationEnabled(false);

// Change colors
        bottomNavigation.setAccentColor(Color.parseColor("#F63D2B"));
        bottomNavigation.setInactiveColor(Color.parseColor("#747474"));

// Force to tint the drawable (useful for font with icon for example)
        bottomNavigation.setForceTint(true);

// Force the titles to be displayed (against Material Design guidelines!)
        bottomNavigation.setForceTitlesDisplay(true);

// Use colored navigation with circle reveal effect
        bottomNavigation.setColored(true);

// Set current item programmatically
        bottomNavigation.setCurrentItem(0);

// Customize notification (title, background, typeface)
        bottomNavigation.setNotificationBackgroundColor(Color.parseColor("#F63D2B"));

// Add or remove notification for each item
        bottomNavigation.setNotification(4, 1);
        bottomNavigation.setNotification(0, 1);

// Set listener
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public void onTabSelected(int position, boolean wasSelected) {
                replacepage(getSupportFragmentManager(), INITIAL_POSITION, position);
            }
        });
    }

    public void replacepage(FragmentManager fragmentManager, int currentPosition, int targetPosition){
        if (currentPosition != targetPosition){
            switch (targetPosition){
                case FEED_CONSTANT:
                    getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
                    toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    feedFragment = FeedFragment.newInstance();
                    fragmentManager.beginTransaction().replace(R.id.page_content, feedFragment).commit();
                    break;
                case ADD_POST:
                    getWindow().setStatusBarColor(getResources().getColor(R.color.secondPrimaryDark));
                    toolbar.setBackgroundColor(getResources().getColor(R.color.pressed));
                    postFragment = PostFragment.newsInstance();
                    fragmentManager.beginTransaction().replace(R.id.page_content, postFragment).commit();
                    break;
                case PROFILE_CONSTANT:
                    getWindow().setStatusBarColor(getResources().getColor(R.color.dark_primar_third));
                    toolbar.setBackgroundColor(getResources().getColor(R.color.dark_blue));
                    profileFragment = ProfileFragment.newInstance();
                    fragmentManager.beginTransaction().replace(R.id.page_content, profileFragment).commit();
                    break;
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
