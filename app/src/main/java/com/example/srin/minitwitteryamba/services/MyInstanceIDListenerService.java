package com.example.srin.minitwitteryamba.services;

import android.content.Intent;

import com.google.android.gms.iid.InstanceIDListenerService;

/**
 * Created by SRIN on 4/26/2016.
 */
public class MyInstanceIDListenerService extends InstanceIDListenerService {

    public static final String TAG = MyInstanceIDListenerService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        Intent intent = new Intent(this, GcmRegistrationServices.class);
        startActivity(intent);
    }
}
