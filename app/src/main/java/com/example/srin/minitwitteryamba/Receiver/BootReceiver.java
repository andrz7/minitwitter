package com.example.srin.minitwitteryamba.Receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.srin.minitwitteryamba.services.UpdaterService;

/**
 * Created by SRIN on 4/22/2016.
 */
public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        context.startService(new Intent(context, UpdaterService.class));
        Log.v("Boot receiver", "onReceived");
    }
}
