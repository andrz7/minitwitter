package com.example.srin.minitwitteryamba.activity;

import android.content.AsyncTaskLoader;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.LoaderManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.dd.CircularProgressButton;
import com.example.srin.minitwitteryamba.R;
import com.example.srin.minitwitteryamba.util.PrefLogin;

import winterwell.jtwitter.Twitter;

public class Login extends AppCompatActivity {

    private CircularProgressButton circularProgressButton;
    private String CONSTANT_USERNAME_ERROR = "Please Fill Your User Name";
    private String CONSTANT_PASSWORD_ERROR = "Please Fill Your Password !";
    private SharedPreferences sp;
    private int CONTANT_TIME = 5000;
    private EditText editUsername, editPassword;
    private String userName, password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
        circularProgressButton.setProgress(0);
        sp = getSharedPreferences("twitter", MODE_PRIVATE);
        editUsername.setText("student");
        editPassword.setText("password");

    }

    private void initView(){
        circularProgressButton = (CircularProgressButton) findViewById(R.id.login_button);
        editUsername = (EditText) findViewById(R.id.edit_user_name);
        editPassword = (EditText) findViewById(R.id.edit_pass);
    }

    public void loginEvent(View v){
        circularProgressButton.setProgress(1);
        userName = editUsername.getText().toString();
        password = editPassword.getText().toString();
        if (userName.isEmpty()) {
            editUsername.setError(CONSTANT_USERNAME_ERROR);
            circularProgressButton.setProgress(-1);
            delay(CONTANT_TIME);
        }
        else if (password.isEmpty()) {
            editPassword.setError(CONSTANT_PASSWORD_ERROR);
            circularProgressButton.setProgress(-1);
            delay(CONTANT_TIME);
        }
        else {
            circularProgressButton.setProgress(50);
            PrefLogin.setUsernameandPassword(Login.this, userName, password);
            circularProgressButton.setProgress(100);
            delayForIntent(CONTANT_TIME - 2000);
        }
    }

    public void delay(int time){
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                circularProgressButton.setProgress(0);
            }
        }, time);
    }

    public void delayForIntent(int time){
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(Login.this, HomeActivity.class));
                finish();
            }
        }, time);

    }
}
