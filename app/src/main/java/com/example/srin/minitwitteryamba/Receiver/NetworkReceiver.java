package com.example.srin.minitwitteryamba.Receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.util.Log;

import com.example.srin.minitwitteryamba.services.UpdaterService;

/**
 * Created by SRIN on 4/22/2016.
 */
public class NetworkReceiver extends BroadcastReceiver {

    private static final String TAG = NetworkReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {

        boolean isNetworkDown = intent.getBooleanExtra(ConnectivityManager.EXTRA_NO_CONNECTIVITY, false);

        if (isNetworkDown){
            Log.i(TAG, "on Receive : Not Connected, stopping UpdaterService");
            context.stopService(new Intent(context, UpdaterService.class));
        }else {
            Log.v(TAG, "onReceive : connected, starting Updater Service");
            context.startService(new Intent(context, UpdaterService.class));
        }
    }
}
