package com.example.srin.minitwitteryamba.services;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.srin.minitwitteryamba.controller.MniTwitterApplication;

import winterwell.jtwitter.TwitterException;

/**
 * Created by SRIN on 4/22/2016.
 */
public class UpdaterService extends Service {

    private String TAG = "UpdaterService";
    public static final String NEW_STATUS_INTENT = "com.example.srin.minitwitteryamba.NEW_STATUS";
    public static final String NEW_STATUS_EXTRA_COUNT = "NEW_STATUS_EXTRA_COUNT";
    private final int DELAY = 60000;
    private boolean runFlag = false;
    private Updater updater;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.updater= new Updater();
        Log.v(TAG, "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!runFlag){
            this.runFlag = true;
            this.updater.start();
            ((MniTwitterApplication)super.getApplication()).setServiceRunning(true);
            Log.v(TAG, "onStarted");
        }
        return Service.START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        this.runFlag = false;
        this.updater.interrupt();
        this.updater = null;
        ((MniTwitterApplication)super.getApplication()).setServiceRunning(false);
        Log.v(TAG, "onDestroyed");
    }

    private class Updater extends Thread{

        static final String RECEIVE_TIMELINE_NOTIFICATION = "com.example.srin.minitwitteryamba.RECEIVE_TIMELINE_NOTIFICATIONS";
        Intent intent;

        public Updater(){
            super("UpdaterService-Updater");
        }

        @Override
        public void run() {
            UpdaterService updaterService = UpdaterService.this;
            while (updaterService.runFlag){
                Log.v(TAG, "Running background thread");
                try {
                    try {
                        MniTwitterApplication twit = (MniTwitterApplication) updaterService.getApplication();
                        int newUpdates = twit.fetchStatusUpdates();
                        if (newUpdates > 0){
                            Log.v(TAG, "We have a new status");
                            intent = new Intent(NEW_STATUS_INTENT);
                            intent.putExtra(NEW_STATUS_EXTRA_COUNT, newUpdates);
                            updaterService.sendBroadcast(intent, RECEIVE_TIMELINE_NOTIFICATION);
                        }
                        Thread.sleep(DELAY);
                    }catch (TwitterException e){
                        Log.e(TAG, "Failed to connect to twitter", e);
                    }
                }catch (InterruptedException e){
                    updaterService.runFlag = false;
                }
            }
        }

    }


}
