package com.example.srin.minitwitteryamba.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.preference.PreferenceManager;

/**
 * Created by SRIN on 4/20/2016.
 */
public class PrefLogin {
    static final String PREF_USER_NAME = "username";
    static final String PREF_PASSWORD = "password";

    public static SharedPreferences getSharedPreferences(Context context){
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    public static void setUsernameandPassword(Context ctx, String username, String password){
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(PREF_USER_NAME, username);
        editor.putString(PREF_PASSWORD, password);
        editor.commit();
    }

}
