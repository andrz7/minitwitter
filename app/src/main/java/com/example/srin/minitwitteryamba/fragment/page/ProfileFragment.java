package com.example.srin.minitwitteryamba.fragment.page;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.srin.minitwitteryamba.R;

/**
 * Created by SRIN on 4/21/2016.
 */
public class ProfileFragment extends Fragment {

    public static ProfileFragment newInstance(){
        return new ProfileFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Context context = new ContextThemeWrapper(getActivity(), R.style.AppTheme);
        LayoutInflater layoutInflater = inflater.cloneInContext(context);
        View v = layoutInflater.inflate(R.layout.fragment_profile, container, false);

        return v;
    }
}
