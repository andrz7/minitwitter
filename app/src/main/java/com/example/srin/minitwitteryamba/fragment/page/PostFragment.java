package com.example.srin.minitwitteryamba.fragment.page;


import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.dd.CircularProgressButton;
import com.example.srin.minitwitteryamba.controller.MniTwitterApplication;
import com.example.srin.minitwitteryamba.R;

import winterwell.jtwitter.Twitter;
import winterwell.jtwitter.TwitterException;

/**
 * Created by SRIN on 4/25/2016.
 */
public class PostFragment extends Fragment {

    private String ERROR_MESSAGE = "Failed to Post - check Preferences";
    private String TAG = PostFragment.class.getSimpleName();
    private EditText post_edit_text;
    private CircularProgressButton circularProgressButton;
    private String getUserPost;
    public static PostFragment newsInstance(){ return new PostFragment(); }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Context context = new ContextThemeWrapper(getActivity(), R.style.AppTheme);
        LayoutInflater layoutInflater = inflater.cloneInContext(context);
        View v = layoutInflater.inflate(R.layout.fragment_post, container, false);
        post_edit_text = (EditText) v.findViewById(R.id.edit_post);
        circularProgressButton = (CircularProgressButton) v.findViewById(R.id.button_for_post);
        circularProgressButton.setProgress(0);
        circularProgressButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUserPost = post_edit_text.getText().toString();
                if (getUserPost.isEmpty()) circularProgressButton.setProgress(-1);
                else {
                    circularProgressButton.setProgress(1);
                    postEvent();
                }
            }
        });
        return v;
    }

    public void postEvent(){
        getUserPost = post_edit_text.getText().toString();
        new PostToTwitter().execute(getUserPost);
    }

    class PostToTwitter extends AsyncTask<String, Integer, String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            circularProgressButton.setProgress(50);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                Log.v(TAG, params[0]);
                MniTwitterApplication twit = (MniTwitterApplication) getActivity().getApplication();
                Twitter.Status status = twit.getTwitter().updateStatus(params[0]);
                return status.text;
            }catch (TwitterException e){
                Log.e(TAG, "Failed to connect to twitter services", e);
                return ERROR_MESSAGE;
            }
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (s.equals(ERROR_MESSAGE))
                circularProgressButton.setProgress(-1);
            else{
                Toast.makeText(getActivity(), s, Toast.LENGTH_SHORT).show();
                circularProgressButton.setProgress(100);
                post_edit_text.setText("");
            }
        }
    }
}
