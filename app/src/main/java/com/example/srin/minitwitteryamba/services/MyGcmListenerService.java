package com.example.srin.minitwitteryamba.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import com.example.srin.minitwitteryamba.R;
import com.example.srin.minitwitteryamba.activity.HomeActivity;
import com.example.srin.minitwitteryamba.activity.MainActivity;
import com.google.android.gms.gcm.GcmListenerService;

/**
 * Created by SRIN on 4/26/2016.
 */
public class MyGcmListenerService extends GcmListenerService {


    public static final String TAG = MyGcmListenerService.class.getSimpleName();

    private static final String KEY_CONTENT = "content";

    private NotificationManager notificationManager;

    @Override
    public void onCreate() {
        super.onCreate();
        Log.v(TAG, "onstart");
    }

    @Override
    public void onMessageReceived(String from, Bundle data) {
        super.onMessageReceived(from, data);
        Log.v(TAG, "onMessageReceived method");
        notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);

        Intent intent = new Intent(this, HomeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        Notification notif = generateBaseNotificationBuilder("You've got Message", data.getString(KEY_CONTENT), pendingIntent)
                .build();
        notif.defaults |= Notification.DEFAULT_SOUND;
        notif.defaults |= Notification.DEFAULT_VIBRATE;
        notificationManager.notify(Integer.MAX_VALUE, notif);
    }

    private NotificationCompat.Builder generateBaseNotificationBuilder(String title, String subTitle, PendingIntent pendingIntent){
        return new NotificationCompat.Builder(this).setSmallIcon(R.drawable.white_twit).setContentTitle(title).
                setContentText(subTitle).setTicker(subTitle).setStyle(new NotificationCompat.BigTextStyle().bigText(subTitle)).
                setAutoCancel(true).setLights(this.getResources().getColor(R.color.colorPrimary), 500, 500).
                setContentIntent(pendingIntent).setAutoCancel(true);
    }


}
