package com.example.srin.minitwitteryamba.controller;

import android.app.Application;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;

import com.example.srin.minitwitteryamba.util.StatusData;

import java.util.List;

import winterwell.jtwitter.Twitter;

/**
 * Created by SRIN on 4/21/2016.
 */
public class MniTwitterApplication extends Application implements SharedPreferences.OnSharedPreferenceChangeListener {

    private final static String TAG = MniTwitterApplication.class.getSimpleName();
    public Twitter twitter;
    private SharedPreferences prefs;
    private StatusData statusData;
    private boolean inTimeLine;
    private boolean serviceRunning;

    @Override
    public void onCreate() {
        super.onCreate();
        this.prefs = PreferenceManager.getDefaultSharedPreferences(this);
        this.prefs.registerOnSharedPreferenceChangeListener(this);
        this.statusData = new StatusData(this);
        Log.i(TAG, "onCreate");
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        this.statusData.close();
        Log.v(TAG, "terminate");
    }

    public synchronized Twitter getTwitter(){
        if (this.twitter == null){
            String username = prefs.getString("username", "");
            String password = prefs.getString("password", "");
            String rootAPI = prefs.getString("apiRoot", "http://yamba.newcircle.com/api");
            Log.v(TAG, username + "-" + password + "-" + rootAPI);
            if (!TextUtils.isEmpty(username) && !TextUtils.isEmpty(password) && !TextUtils.isEmpty(rootAPI)){
                this.twitter = new Twitter(username, password);
                this.twitter.setAPIRootUrl(rootAPI);
            }
        }
        return this.twitter;
    }

    public boolean startOnBoot(){
        return this.prefs.getBoolean("startonboot", false);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        this.twitter = null;
    }

    public boolean isServiceRunning(){
        return serviceRunning;
    }

    public void setServiceRunning(boolean serviceRunning){
        this.serviceRunning = serviceRunning;
    }

    public boolean isInTimeLine(){
        return inTimeLine;
    }

    public void setIntTimeLine(boolean inTimeLine){
        this.inTimeLine = inTimeLine;
    }

    public StatusData getStatusData(){
        return statusData;
    }

    public SharedPreferences getPrefs(){
        return prefs;
    }

    public synchronized int fetchStatusUpdates(){
        Log.v(TAG, "Fetching status update");
        Twitter twitter = this.getTwitter();
        if (twitter == null){
            Log.v(TAG, "Fetching status Update");
            return 0;
        }
        try{
            List<Twitter.Status> statusesUpdate = twitter.getFriendsTimeline();
            long latestStatusCreatedAtTime = this.getStatusData().getLatestStatusCreatedAtTime();
            Log.v(TAG, "latestStatusCreatedAtTime : " + latestStatusCreatedAtTime);
            int count = 0;
            ContentValues cv = new ContentValues();
            for (Twitter.Status status : statusesUpdate){
                cv.put(StatusData.C_ID, status.getId());
                long createdAt = status.getCreatedAt().getTime();
                Log.v(TAG, StatusData.C_ID + " : " + status.getId() + StatusData.C_CREATED_AT
                        + " : " + createdAt + StatusData.C_TEXT + " : " + status.getText()
                        + StatusData.C_USER + " ; " + status.getUser().getName());
                cv.put(StatusData.C_CREATED_AT, createdAt);
                cv.put(StatusData.C_TEXT, status.getText());
                cv.put(StatusData.C_USER, status.getUser().getName());
                cv.put(StatusData.C_LINK, String.valueOf(status.getUser().getProfileImageUrl()));
                Log.v(TAG, "Got update with id " + status.getId() + " .Saving");
                this.getStatusData().insertOrIgnore(cv);
                if (latestStatusCreatedAtTime < createdAt)
                    count++;
            }
            Log.v(TAG, count > 0 ? "Got " + count + " status Update" : "No new Status Updates");
            return count;
        }catch (RuntimeException e){
            Log.e(TAG, "Failed to fetch status update " + e);
            return 0;
        }
    }
}
