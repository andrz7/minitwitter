package com.example.srin.minitwitteryamba.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by SRIN on 4/21/2016.
 */
public class StatusData {

    private static final String TAG = StatusData.class.getSimpleName();
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "timeline.db";
    static final String TABLE = "timeline";

    public static final String C_ID = "_id";
    public static final String C_CREATED_AT = "created_at";
    public static final String C_TEXT = "txt";
    public static final String C_USER = "user";
    public static final String C_LINK = "picture";

    private static final String GET_ALL_ORDER_BY = C_CREATED_AT + " DESC";

    private static final String[] MAX_CREATED_AT_COLUMNS = { "max(" + StatusData.C_CREATED_AT + ")" };

    private static final String[] DB_TEXT_COLUMN = {C_TEXT};

    final DbHelper dbHelper;

    public StatusData(Context context) {
        this.dbHelper = new DbHelper(context);
        Log.i(TAG, "initialized data");
    }

    public void close(){
        this.dbHelper.close();
    }

    public void insertOrIgnore(ContentValues cv){
        Log.v(TAG, "insert or ignore on " + cv);
        SQLiteDatabase db = this.dbHelper.getWritableDatabase();
        try {
            db.insertWithOnConflict(TABLE, null, cv, SQLiteDatabase.CONFLICT_IGNORE);
        }finally {

        }
    }

    public Cursor getStatusUpdate(){
        SQLiteDatabase db = this.dbHelper.getReadableDatabase();
        return db.query(TABLE, null, null, null, null, null, GET_ALL_ORDER_BY);
    }

    public long getLatestStatusCreatedAtTime(){
       SQLiteDatabase db = this.dbHelper.getReadableDatabase();
        try {
            Cursor cursor = db.query(TABLE, MAX_CREATED_AT_COLUMNS, null, null, null, null, null);
            try {
                return cursor.moveToNext() ? cursor.getLong(0) : Long.MIN_VALUE;
            } finally {}
        }finally {}
    }

    public void delete(){
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete(TABLE, null, null);
    }

    class DbHelper extends SQLiteOpenHelper{

        public DbHelper(Context context){
            super(context, DB_NAME, null, DB_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.v(TAG, "creating database : " + DB_NAME);
            db.execSQL("create table " + TABLE + "(" + C_ID + " integer primary key,"
                    + C_CREATED_AT + " integer," + C_USER + " text, " + C_TEXT
                    + " text, " + C_LINK  + " text);");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("drop table" + TABLE);
            this.onCreate(db);
        }
    }
}
