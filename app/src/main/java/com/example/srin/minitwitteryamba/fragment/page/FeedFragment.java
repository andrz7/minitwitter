package com.example.srin.minitwitteryamba.fragment.page;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.example.srin.minitwitteryamba.R;
import com.example.srin.minitwitteryamba.controller.MniTwitterApplication;
import com.example.srin.minitwitteryamba.services.UpdaterService;
import com.example.srin.minitwitteryamba.util.StatusData;
import com.mikhaellopez.circularimageview.CircularImageView;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by SRIN on 4/21/2016.
 */
public class FeedFragment extends Fragment {
    private static final String[] FROM = {StatusData.C_CREATED_AT, StatusData.C_USER, StatusData.C_TEXT, StatusData.C_LINK};
    private static final int[] TO = {R.id.text_created_at, R.id.text_user, R.id.text_post, R.id.profile_picture};
    static final String SEND_TIMELINE_NOTIFICATIONS = "com.example.srin.minitwitteryamba.SEND_TIMELINE_NOTIFICATIONS";
    public static final String URL = "http://datahelper.comlu.com/access.php";
    private static final String KEY_TOKEN = "token";
    private SwipeRefreshLayout swipeRefreshLayout;
    private TextView createdAt, userName, userPost;
    private SimpleCursorAdapter cursorAdapter;
    private CircularImageView profilePicture;
    private TimeLineReceiver receiver;
    private IntentFilter filter;
    private ListView listView;
    private Cursor cursor;

    public static FeedFragment newInstance(){ return new FeedFragment(); }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Context context = new ContextThemeWrapper(getActivity(), R.style.AppTheme);
        LayoutInflater layoutInflater = inflater.cloneInContext(context);
        View v = layoutInflater.inflate(R.layout.fragment_feed, container, false);

        listView = (ListView) v.findViewById(R.id.list_time_line);
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.swipeContainer);
        receiver = new TimeLineReceiver();
        filter = new IntentFilter(UpdaterService.NEW_STATUS_INTENT);
        listView.setEmptyView(v.findViewById(R.id.warning));
        return v;
    }

    @Override
    public void onStop() {
        getActivity().unregisterReceiver(receiver);
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                setUpList();
            }
        });
        this.setUpList();
        super.getActivity().registerReceiver(receiver
                , filter, SEND_TIMELINE_NOTIFICATIONS, null);
//        cursorAdapter.notifyDataSetChanged();
    }

    public void setUpList(){
        MniTwitterApplication twit = (MniTwitterApplication) super.getActivity().getApplication();
        this.cursor = twit.getStatusData().getStatusUpdate();
        getActivity().startManagingCursor(this.cursor);
//        cursorAdapter = new SimpleCursorAdapter(getActivity(), R.layout.row, cursor, FROM, TO);
//        cursorAdapter.setViewBinder(VIEW_BINDER);
        MyCursorAdapter myCursorAdapter = new MyCursorAdapter(getActivity(), R.layout.row, cursor, FROM, TO);
        listView.setAdapter(myCursorAdapter);
        myCursorAdapter.notifyDataSetChanged();
        if(swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
    }

    static final SimpleCursorAdapter.ViewBinder VIEW_BINDER = new SimpleCursorAdapter.ViewBinder() {
        @Override
        public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
            if (view.getId() != R.id.text_created_at)
                return false;
            long timeStamp = cursor.getLong(columnIndex);
            CharSequence relTime = DateUtils.
                    getRelativeTimeSpanString(view.getContext(), timeStamp);
            ((TextView) view).setText(relTime);
            return true;
        }
    };

    public class TimeLineReceiver extends BroadcastReceiver {
        private final String TAG = TimeLineReceiver.class.getSimpleName();

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.v(TAG, "onReceive");
            try{
                if (intent == null) return;
                if (context == null) return;
                final int count = intent.getExtras().getInt("NEW_STATUS_EXTRA_COUNT");
                Log.v(TAG, String.valueOf(count));
                setUpList();
                sendRequest(context);
            }catch (Exception e){Log.e(TAG, e.getMessage());}

        }

        public void sendRequest(Context context){
            SharedPreferences preferences = context.getSharedPreferences("token", Context.MODE_APPEND);
            final String token = preferences.getString("kunci", "");
            Log.v(TAG, token);
            StringRequest stringRequest = new StringRequest(Request.Method.POST, URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            System.out.println(response.substring(0,100));
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    System.out.println("Something went wrong!");
                    error.printStackTrace();
                }
            }){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    params.put(KEY_TOKEN, token);
                    return params;
                }
            };

            Volley.newRequestQueue(context).add(stringRequest);
        }
    }

    class MyCursorAdapter extends SimpleCursorAdapter{

        public MyCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to) {
            super(context, layout, c, from, to);
        }

        @Override
        public void bindView(View view, Context context, Cursor cursor) {
            createdAt = (TextView) view.findViewById(R.id.text_created_at);
            profilePicture = (CircularImageView) view.findViewById(R.id.profile_picture);
            userName = (TextView) view.findViewById(R.id.text_user);
            userPost = (TextView) view.findViewById(R.id.text_post);

            long timeStamp = cursor.getLong(cursor.getColumnIndex(StatusData.C_CREATED_AT));
            String link = cursor.getString(cursor.getColumnIndex(StatusData.C_LINK));
            String name = cursor.getString(cursor.getColumnIndex(StatusData.C_USER));
            String textPost = cursor.getString(cursor.getColumnIndex(StatusData.C_TEXT));

            CharSequence relTime = DateUtils.
                    getRelativeTimeSpanString(view.getContext(), timeStamp);
            createdAt.setText(relTime);

            Glide.with(context).load(link).into(profilePicture);
            userName.setText(name);
            userPost.setText(textPost);
        }
    }
}
