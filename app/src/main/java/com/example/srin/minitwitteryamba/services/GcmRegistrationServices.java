package com.example.srin.minitwitteryamba.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;

/**
 * Created by SRIN on 4/26/2016.
 */
public class GcmRegistrationServices extends IntentService {

    private static final String TAG = GcmRegistrationServices.class.getSimpleName();
    private static final String SENDER_ID = "549201515071";

    public GcmRegistrationServices() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            synchronized (TAG){
                InstanceID instanceID = InstanceID.getInstance(this);
                String token = instanceID.getToken(SENDER_ID,
                        GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                SharedPreferences preferences = getSharedPreferences("token", MODE_APPEND);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putString("kunci", token);
                editor.commit();
                Log.v(TAG, "GCM registration token " + token);
            }
        }catch (Exception e){
            Log.v(TAG, "Failed to complete token refresh", e);
        }
    }
}
